import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from "react-redux";
const NavHeader =  (props) => {
  return <nav >
    <Link style={{
      color: props.theme
    }}
    onClick={props.history.goBack} to={'#'}>Back</Link>
  </nav>
}
const mapStateToProps = (state /*, ownProps*/) => {
  return {
    theme: state.theme
  };
};
export default withRouter(connect(mapStateToProps)(NavHeader));