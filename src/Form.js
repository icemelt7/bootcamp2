import React, { Component } from 'react';

class Form extends Component {
  state = {
    name: "",
    email: ""
  }
  updateVal = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  render(){
    return <form>
      <input type="text" name="name" onChange={this.updateVal} />
      <input type="email" name="email" onChange={this.updateVal} />
      <br />
      <NonEmpty message="My Name is" value={this.state.name} />
      <br />
      <NonEmpty message="My Email is" value={this.state.email} />
    </form>
  }
}
function NonEmpty(props){
  return props.value && <div>{props.message}: {props.value}</div>
}
export default Form;