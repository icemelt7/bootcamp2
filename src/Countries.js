import React, {Component} from 'react';
import loader from './images/loading.gif';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";

class Countries extends Component {
  state = {
    countries: []
  }
  componentDidMount(){
    let url = "";
    if (this.props.match.params.regioncode){
      url = `https://restcountries.eu/rest/v2/region/${this.props.match.params.regioncode}`
    }else{
      url = 'https://restcountries.eu/rest/v2/all'
    }
    fetch(url)
      .then((response) => {
        return response.json();
      })
      .then((myJson) => {
        this.setState({
          countries: myJson.slice(0, 5)
        })
    });
  }
  
  render(){
    return <div>
      {
        this.state.countries.length === 0 ? <Loading />:
        <ul><CountriesListContainer countries={this.state.countries}/></ul>
      }
    </div>
  }
}
const CountriesList = (props) => {
  return props.countries.map((country, i) => {
    return <li key={i}><Link style={{
      color: props.theme
    }} to={`/country/${country.alpha2Code}`}>{country.name}</Link></li>
  })
}
const mapStateToProps = (state /*, ownProps*/) => {
  return {
    theme: state.theme
  };
};
const CountriesListContainer = connect(mapStateToProps)(CountriesList);


const Loading = (props) => {
  return <p><img src={loader} alt="Loading"/></p>
}
export default Countries