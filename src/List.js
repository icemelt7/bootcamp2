import React, { Component } from 'react';

class List extends Component {
  state = {
    _list: [
      {
        name: "Apple",
        price: "12.22"
      },
      {
        name: "Orange",
        price: "15.22"
      },
      {
        name: "Peach",
        price: "11.22"
      },
      {
        name: "Some Random froot",
        price: "5.22"
      }
    ]
  }
  render(){
    return <div>A List
      {this.state._list.map((listItem, i) => {
        return <ListItem key={i} item={listItem} />
      })}
    </div>
  }
}

const ListItem = (props) => {
  return <div>
    <span>{props.item.name}</span>
    <span>{props.item.price}</span>
  </div>
}
export default List;