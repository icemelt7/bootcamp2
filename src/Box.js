import React from 'react';

const Box = (props) => {
  return <div className="box">
  <a href="https://youtu.be/s6zR2T9vn2c" className="image fit">
  <img src={props.image} alt="" /></a>
  <div className="inner">
    <h3>Nascetur nunc varius commodo</h3>
    <p>Interdum amet accumsan placerat commodo ut amet aliquam blandit nunc tempor lobortis nunc non. Mi accumsan.</p>
    <a href="https://youtu.be/s6zR2T9vn2c" className="button fit" data-poptrox="youtube,800x400">Watch</a>
  </div>
</div>
}

export default Box;