import React from 'react';
import ReactDOM from 'react-dom';
import App from './Enviornment';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from "react-redux";
import { createStore } from 'redux'
import countriesApp from './reducers'
const store = createStore(countriesApp)
ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>
  , document.getElementById('root'));
