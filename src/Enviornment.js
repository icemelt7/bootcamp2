import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom'
import LoginForm from './LoginForm';
import RegistrationForm from './RegistrationForm';
import Dashboard from './Dashboard';
import './App.css'
class App extends Component {
  state = {
    loggedInStatus: false,
    token: null,
    userDetails: null
  }
  componentDidMount(){
    if (localStorage.getItem('token') !== null){
      this.setState({
        loggedInStatus: true,
        token: localStorage.getItem('token')
      })
    }
  }
  handleLogin = (loginData) => {
    if (loginData.name){
      localStorage.setItem("token", loginData.token);
      this.setState({
        loggedInStatus: true,
        userDetails: loginData,
        token: loginData.token
      })
    }
  }
  logout = () => {
    localStorage.removeItem("token");
    this.setState({
      loggedInStatus: false,
      userDetails: null,
      token: null
    })
  }
  render() {
    return (
      <div className="container">
        <div>
          
          {this.state.loggedInStatus === false && <div>
            <Link to={'/login'}>Login</Link> --  
            <Link to={'/register'}>Register</Link> -- 
          </div>}
          
          {this.state.loggedInStatus === true && <div>
            <Link to={'/dashboard'}>Dashboard</Link> -- 
            <Link to={'#'} onClick={this.logout}>Logout</Link>
            </div>
          }
        </div>
        
        <Switch>
          <Route path="/login" 
            render={(props) => <LoginForm {...props} 
            loggedInStatus={this.state.loggedInStatus}
            handleLogin={this.handleLogin}/>}
          />
          <Route path="/register" 
            loggedInStatus={this.state.loggedInStatus}
            render={(props) => <RegistrationForm {...props}/>}
          />
          <Route path="/dashboard" 
            render={(props) => <Dashboard {...props} 
            token={this.state.token}
            logout={this.logout} />}
          />
          <Route path="/" 
            render={(props) => <Dashboard {...props} 
            logout={this.logout}
            token={this.state.token} />}
          />
        </Switch>
      </div>
    );
  }
}

export default App;