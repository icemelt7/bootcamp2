const initialState = {
  theme: "red"
}
function countriesApp(state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_THEME':
      return {
        ...state,
        theme: action.theme
      }
    default:
      return state
  }
}
export default countriesApp