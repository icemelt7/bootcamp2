import React from 'react';

const Thumbnails = (props) => {
  return <div className="thumbnails">
    {props.children}
  </div>
}

export default Thumbnails;