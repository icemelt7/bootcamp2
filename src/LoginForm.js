import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
class LoginForm extends Component {
  state = {
    error: false
  }
  login = () => {
    fetch('https://firstone20181124.herokuapp.com/login',{
      method: 'post',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password
      })
    }).then((response) => {
      response.json().then((jsonResponse) => {
        this.props.handleLogin(jsonResponse)
      })
    })
  }
  onChange = (input) => {
    this.setState({
      [input.target.name]: input.target.value
    })
  }
  onSubmit = (event) => {
    event.preventDefault();
    return true;
  }
  render() {
    if (this.props.loggedInStatus){
      return <Redirect to={'/dashboard'} />
    }
    return <form onSubmit={this.onSubmit}>
      <input type="email" name="email" onChange={this.onChange}/>
      <input type="password" name="password" onChange={this.onChange}/>
      <input type="submit" value="Login" onClick={this.login}/>

      {this.state.error && <div style={{backgroundColor: "red"}}>Wrong Password</div>}
      {this.state.name && <div style={{backgroundColor: "green"}}>Hello {this.state.name}</div>}
      
    </form>
  }
}
export default LoginForm