import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class Dashboard extends Component {
  state = {
    status: "pending",
    data: null
  }
  componentDidMount(){
    // Assume user is logged in
    if (localStorage.getItem('token') == null){
      this.props.logout();
      this.setState({
        status: "unauthorized"
      })
      return;
    }
    
    fetch('https://firstone20181124.herokuapp.com/dashboard',{
      headers: new Headers({
        'Authorization': `${this.props.token}`
      })
    }).then((response) => {
      response.json().then((jsonResponse) => {
        this.setState({
          status: "loggedin",
          data: jsonResponse
        })
      }).catch((err) => {
        this.props.logout();
        this.setState({
          status: "unauthorized"
        })
      })
    })
  }
  
  render() {
    return <div>
      {this.state.status === "pending" && "Pending"}
      {this.state.status === "loggedin" && <div>
        {this.state.data.myFriends.map(friend => 
          <div key={friend.name}>{friend.name}</div>)}
      </div>}
      {this.state.status === "unauthorized" && <Redirect to={'/login'} />}
    </div>
  }
}
export default Dashboard