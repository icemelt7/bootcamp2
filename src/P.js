import React, { Component } from 'react';

class P extends Component {
  state = {
    color: "green"
  }
  changeColor = () => {
    const colors = ["red", "green", "pink", "blue"]
    const newColor = colors[Math.floor(Math.random() * colors.length)]
    this.setState({
      color: newColor
    })
  }
  render(){
    return <div style={{
      color: this.state.color
    }}>I am funny bird
    <br />
    <button onClick={this.changeColor}>Please turn me into something else</button>

    </div>
  }
}

export default P