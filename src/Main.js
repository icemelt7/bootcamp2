import React from 'react';

const Main = (props) => {
  return <div id="main">
    <div class="inner">
      {props.children}
    </div>
  </div>
}

export default Main;