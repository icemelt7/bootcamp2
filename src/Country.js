import React, {Component} from 'react';
import { Link } from 'react-router-dom'
class Country extends Component {
  state = {
    country: {},
    loaded: false
  }
  componentDidMount(){
    const code = this.props.match.params.code;
    fetch(`https://restcountries.eu/rest/v2/alpha/${code}`)
      .then(function(response) {
        return response.json();
      })
      .then((myJson) => {
        this.setState({
          country: myJson,
          loaded: true
        })
    });
  }

  render(){
    if (!this.state.loaded){
      return "Loading";
    }
    const { name, capital, region, languages } = this.state.country;
    return <div>
      <h1 style={{
        color: this.props.theme
      }}>{name}</h1>
      <h2>Capital City is: {capital}</h2>
      <ul>
        Languages spoken:
        {languages.map( (language, i) => <li key={i}>{language.nativeName}</li>)}
      </ul>
      <small>Region: <Link to={`/region/${region}`}>{region}</Link></small>
    </div>
  }
}

export default Country